package com.dynamicExcel.dynamicExcel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.mongodb.client.MongoDatabase;
import com.mongodb.MongoClient;

@SpringBootApplication
public class DynamicExcelApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamicExcelApplication.class, args);
		
	}

}
