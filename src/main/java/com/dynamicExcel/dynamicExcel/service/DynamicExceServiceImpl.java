package com.dynamicExcel.dynamicExcel.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

@Service
public class DynamicExceServiceImpl implements DynamicExcelService {

	
	Workbook workbook;

	public String uploadDir = "/tmp";

	public void uploadFile(MultipartFile file) {

        try {
        	System.out.println(file.getName());
            Path copyLocation = Paths
                .get(uploadDir + File.separator + StringUtils.cleanPath(file.getOriginalFilename()));
            System.out.println(copyLocation.toString());
            Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Could not store file " + file.getOriginalFilename()
                + ". Please try again!");
        }
    }

	public List<?> getExcelDataAsList() {

		List<String> list = new ArrayList<String>();

		DataFormatter dataFormatter = new DataFormatter();
		try {
			workbook = WorkbookFactory.create(new File(uploadDir+"/dynamicData.xlsx"));
			 MongoClient mongo = new MongoClient( "localhost" , 27017 );
		      //Connecting to the database
		      MongoDatabase database = mongo.getDatabase("multipleExcelFile");
		
				System.out.println("-------Workbook has '" + workbook.getNumberOfSheets() + "' Sheets-----");
				int j =0;
				for(Sheet sheet: workbook) {
					int noOfColumns = sheet.getRow(0).getLastCellNum();
					database.createCollection("Sheet0"+j);
					MongoCollection<Document> collection= database.getCollection("Sheet0"+j);
					int noOfRows = sheet.getPhysicalNumberOfRows();
					System.out.println("-------Sheet has '"+noOfColumns+"' columns------"+"----and---"+noOfRows+" rows----");
	
					boolean flag=false;
					List<String> fields = new ArrayList<>();
					for (Row row : sheet) {
						Document doc = new Document();
						doc.append("_id", new ObjectId());
						int i=0;
						
						for(Cell cell: row) {
							String cellValue = dataFormatter.formatCellValue(cell);
							if(flag) {
								doc.append(list.get(i), cellValue);
								i++;
							}else {
								list.add(cellValue.strip());
							}
						}
						collection.insertOne(doc);
								
						flag=true;
					}

						j++;
						
//						}
				}
		} catch (EncryptedDocumentException | IOException e) {
			e.printStackTrace();
		}
				try {
					workbook.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

				return list;
			} 
    
}
