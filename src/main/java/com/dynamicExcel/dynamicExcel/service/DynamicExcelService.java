package com.dynamicExcel.dynamicExcel.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;


public interface DynamicExcelService {

	void uploadFile(MultipartFile file);

	List<?> getExcelDataAsList();

}
